# Load required libraries
gulp        = require 'gulp'
sass        = require 'gulp-sass'
gutil       = require 'gulp-util'
sass        = require 'gulp-sass'
coffee      = require 'gulp-coffee'
concat      = require 'gulp-concat'
rename      = require 'gulp-rename'
minifyCss   = require 'gulp-minify-css'

gulp.task 'default', ['scss', 'coffee']

gulp.task 'scss', ->
  gulp.src ['./src/css/**/*.scss']
    .pipe sass()
    .pipe minifyCss { keepSpecialComments: 0 }
    .pipe rename { extname: '.min.css' }
    .pipe gulp.dest './dist/css/'

gulp.task 'coffee', ->
  gulp.src ['./src/js/**/*.coffee']
    .pipe coffee()
    .pipe gulp.dest './dist/js/'
